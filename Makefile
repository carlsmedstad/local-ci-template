MYPATH := $(dir $(lastword $(MAKEFILE_LIST)))

.PHONY: check
check:
	$(MYPATH)findsh | xargs --no-run-if-empty shellcheck
	shfmt -d .

.PHONY: fix
fix:
	shfmt -w .

.PHONY: python-env
python-env:
	podman run --rm -it alpine:3
